/*
 * Copyright (C) 2013 NASA PhoneSat
 *               2017 Fabian P. Schmidt <kerel-fs@gmx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
"use strict";

//function satellite_t(){}
// ////////////////////////////////////////////////////
// Latitude and longitude orbit plot on the earth map
// ////////////////////////////////////////////////////

Sat_t.prototype.mapInit = function(map) {

	this.mapSvg = document.getElementById('map');
	if (!this.mapSvg) {
		return;
	}

	predict_calc(this, qth, julian_date(new Date()));

	// Create satellite dot
	//this.mapSat = L.circleMarker(
	//		[this.ssplat, this.ssplon],
	//		{
	//			radius: 3,
	//			stroke: false,
	//			fill: true,
	//			fillColor: 'rgb(0,200,0)',
	//			fillOpacity: 1,
	//		}
	//);
        this.mapSat = L.marker([this.ssplat, this.ssplon],
                { icon: L.icon(
                        {
                                iconUrl: "design_1.png",
                                iconSize: [50, 50],
                                iconAnchor: [25, 25],
                        }),
                }
        );
        this.mapSat.bindTooltip(this.name,
                {
                        permanent: true,
                        direction: 'right',
                        offset: L.point( {x: 20, y: 0}),
                        opacity: 0.9,
                }
        );

	// Create satellite footprint
	this.mapFootprint = L.polygon(
			[],
			{
				stroke: true,
				color: this.color,
				weight: 2,
				fill: true,
			}
	);

	// Create satellite orbit in 3 parts to handle antemeridian crossing
        this.mapOrbit_1 = L.polyline( [], { color: this.color, weight: 4 });
        this.mapOrbit_2 = L.polyline( [], { color: this.color, weight: 3 });
        this.mapOrbit_3 = L.polyline( [], { color: this.color, weight: 2 });

        // place OSM/Monaco at 43°43'44.2"N 7°24'50.4"E: 43.728944, 7.413988
        this.mapOSM = L.circleMarker([43.728944, 7.413988],
                      {
                              radius: 3,
                              stroke: false,
                              fill: true,
                              fillColor: 'rgb(255, 0, 0)',
                              fillOpacity: 1,
                      }
        );
        this.mapOSM.bindTooltip("Monaco",
                {
                        permanent: true,
                        direction: 'right',
                        opacity: 0.9,
                        className: 'monaco_tooltip',
                }
        );
        //this.mapOSM.setTooltipContent("<img src=\"logo_drapeau.png\">");

        // Create the layer
        var layer = L.layerGroup([
                this.mapSat, this.mapFootprint,
                this.mapOrbit_1, this.mapOrbit_2, this.mapOrbit_3,
                this.mapOSM,
        ]);
        layer.addTo(map);

        layers_control.addOverlay(layer, this.name);
};

Sat_t.prototype.mapRefresh = function() {
	if (!this.mapSvg) {
		return;
	}

	// Refresh satellite dot
	this.mapSat.setLatLng([this.ssplat, this.ssplon]);

	// Refresh satellite footprint
	//{
	//	var azi;
	//	//var msx, msy, ssx, ssy;
	//	var ssplat, ssplon, beta, azimuth, num, dem;
	//	//var rangelon, rangelat, mlon;

	//	var geo = new Geodetic_t();

	//	/* Range circle calculations.
	//	 * Borrowed from gsat 0.9.0 by Xavier Crehueras, EB3CZS
	//	 * who borrowed from John Magliacane, KD2BD.
	//	 * Optimized by Alexandru Csete and William J Beksi.
	//	 */
	//	ssplat = radians(this.ssplat);
	//	ssplon = radians(this.ssplon);
	//	beta = (0.5 * this.footprint) / xkmper;

	//	var gn = "", gp = "", g = "";
	//	var pos_overlap = false;
	//	var neg_overlap = false;
	//	var points = [];

	//	for (azi = 0; azi < 360; azi += 2) {
	//		azimuth = de2ra * azi;
	//		geo.lat = asin(sin(ssplat) * cos(beta) + cos(azimuth) * sin(beta)
	//				* cos(ssplat));
	//		num = cos(beta) - (sin(ssplat) * sin(geo.lat));
	//		dem = cos(ssplat) * cos(geo.lat);

	//		if (azi == 0 && (beta > pio2 - ssplat))
	//			geo.lon = ssplon + pi;

	//		else if (azi == 180 && (beta > pio2 + ssplat))
	//			geo.lon = ssplon + pi;

	//		else if (fabs(num / dem) > 1.0)
	//			geo.lon = ssplon;

	//		else {
	//			if ((180 - azi) >= 0)
	//				geo.lon = ssplon - arccos(num, dem);
	//			else
	//				geo.lon = ssplon + arccos(num, dem);
	//		}

	//		points.push([degrees(geo.lat), degrees(geo.lon)]);
	//	}

	//	this.mapFootprint.setLatLngs(points);
	//}

	// Draw satellite orbit
	{
                var orbit_1 = [];
                var orbit_2 = [];
                var orbit_3 = [];
		var t = new Date();

		var previous = 0;
                var orbit_idx = 1;

		for (var i = 0; i < COUNT; i++) {
			predict_calc(this, qth, julian_date(t));

			if (Math.abs(this.ssplon - previous) > 180)
				// orbit crossing -PI, PI
				orbit_idx += 1;

                        if (orbit_idx == 1)
                                orbit_1.push([this.ssplat, this.ssplon]);
                        if (orbit_idx == 2)
                                orbit_2.push([this.ssplat, this.ssplon]);
                        if (orbit_idx == 3)
                                orbit_3.push([this.ssplat, this.ssplon]);

			previous = this.ssplon;

			// Increase time for next point
			t.setTime(t.getTime() + STEP);
		}

		this.mapOrbit_1.setLatLngs(orbit_1);
		this.mapOrbit_2.setLatLngs(orbit_2);
		this.mapOrbit_3.setLatLngs(orbit_3);
	}

        // Update telemetry
        var t = new Date();
        //console.log(t.toISOString());
        //console.log(t.getTime());
        var date = julian_date(t);
        //console.log(date);
        var pass;

        var monaco_pass = get_pass(this, monaco_gnd_station, date, 0.0, pass);
        //console.log("monaco pass");
        //console.log(monaco_pass.tca);
        monaco_pass = new Date(Date_Time(monaco_pass.tca));
        //console.log(monaco_pass.getTime());
        //console.log(monaco_pass.toISOString());
        const monaco_pass_delta = (monaco_pass - t) / 1000;
        const monaco_pass_delta_sec = monaco_pass_delta % 60;
        const monaco_pass_delta_min = ((monaco_pass_delta - monaco_pass_delta_sec) / 60) % 60;
        const monaco_pass_delta_hr = ((monaco_pass_delta - monaco_pass_delta_sec - monaco_pass_delta_min * 60) / 3600) % 3600;

        const monaco_pass_month = monaco_pass.getUTCMonth() + 1;

        this.mapSat.setTooltipContent(
                "<table>" +
                "<tr style=\"color: #ffffff\">" +
                "</tr>" +
                "<th><img src=\"OSM_logo.png\" alt=\"\" border=0 height=15 width=25></img></th>" +
                "<th colspan=\"2\">" + this.name + "</th>" +
                "<th><img src=\"Monaco_flag.svg\" alt=\"\" border=0 height=15 width=25></img></th>" +
                "<tr style=\"color: #000000\">" +
                "<td style=\"background-color: #d0d0d0\">altitude</td>" +
                "<td style=\"background-color: #d0d0d0\">velocity</td>" +
                "<td style=\"background-color: #d0d0d0\">latitude</td>" +
                "<td style=\"background-color: #d0d0d0\">longitude</td>" +
                "</tr>" +
                "<tr style=\"color: #000000\">" +
                "<td>" + this.alt.toFixed(1) + "km</td>" +
                "<td>" + this.velo.toFixed(1) + "km/s</td>" +
                "<td>" + this.ssplat.toFixed(3) + "°</td>" +
                "<td>" + this.ssplon.toFixed(3) + "°</td>" +
                "</tr>" +
                "<tr style=\"color: #000000\">" +
                "<td colspan=\"2\">next pass (UTC)</td>" +
                //"<td colspan=\"2\">" + monaco_pass.toISOString() + "</td>" +
                "<td colspan=\"2\">" +
                monaco_pass.getUTCFullYear() + "-" +
                monaco_pass_month.toString().padStart(2, "0") + "-" +
                monaco_pass.getUTCDate() + " " +
                monaco_pass.getUTCHours() + ":" +
                monaco_pass.getUTCMinutes() +
                "</td>" +
                "</tr>" +
                "<tr style=\"color: #000000\">" +
                "<td colspan=\"2\">over Monaco</td>" +
                "<td colspan=\"2\">" + "in " +
                monaco_pass_delta_hr.toFixed(0) + "h " +
                monaco_pass_delta_min.toFixed(0) + "m " +
                monaco_pass_delta_sec.toFixed(0) + "s" +
                "</td>" +
                "</tr>" +
                "</table>"
        );
};

// ////////////////////////////////////////////////////////////////
// Periodic refresh loop
// ////////////////////////////////////////////////////////////////

function refresh() {
	var jt = julian_date(new Date());

	// Compute current satellites longitude and latitude
	for (var i = 0; i < sats.length; i++) {
		predict_calc(sats[i], qth, jt);
		sats[i].mapRefresh();
	}
}
