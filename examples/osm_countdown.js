/*
 * Copyright (C) 2013 NASA PhoneSat
 *               2017 Fabian P. Schmidt <kerel-fs@gmx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
"use strict";

//function satellite_t(){}
// ////////////////////////////////////////////////////
// Latitude and longitude orbit plot on the earth map
// ////////////////////////////////////////////////////

Sat_t.prototype.mapRefresh = function() {
        // Update telemetry
        var t = new Date();
        //console.log(t.toISOString());
        //console.log(t.getTime());
        var date = julian_date(t);
        //console.log(date);
        var pass;

        var monaco_pass = get_pass(this, monaco_gnd_station, date, 0.0, pass);
        //console.log("monaco pass");
        //console.log(monaco_pass.tca);
        monaco_pass = new Date(Date_Time(monaco_pass.tca));
        //console.log(monaco_pass.getTime());
        //console.log(monaco_pass.toISOString());
        const monaco_pass_delta = (monaco_pass - t) / 1000;
        const monaco_pass_delta_sec = monaco_pass_delta % 60;
        const monaco_pass_delta_min = ((monaco_pass_delta - monaco_pass_delta_sec) / 60) % 60;
        const monaco_pass_delta_hr = ((monaco_pass_delta - monaco_pass_delta_sec - monaco_pass_delta_min * 60) / 3600) % 3600;

        const monaco_pass_month = monaco_pass.getUTCMonth() + 1;


        document.getElementById(this.name + "_date").innerHTML =
                monaco_pass.getUTCFullYear() + "-" +
                monaco_pass_month.toString().padStart(2, "0") + "-" +
                monaco_pass.getUTCDate() + " " +
                monaco_pass.getUTCHours() + ":" +
                monaco_pass.getUTCMinutes();

        document.getElementById(this.name + "_cd").innerHTML =
                monaco_pass_delta_hr.toFixed(0) + "h " +
                monaco_pass_delta_min.toFixed(0) + "m " +
                monaco_pass_delta_sec.toFixed(0) + "s";
};

// ////////////////////////////////////////////////////////////////
// Periodic refresh loop
// ////////////////////////////////////////////////////////////////

function refresh() {
	var jt = julian_date(new Date());

	// Compute current satellites longitude and latitude
	for (var i = 0; i < sats.length; i++) {
		predict_calc(sats[i], qth, jt);
		sats[i].mapRefresh();
	}
}
